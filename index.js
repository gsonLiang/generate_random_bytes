const crypto = require('crypto');
const genSpecificLengthRandom = (bytes) => {
  return crypto.randomBytes(bytes).toString('hex')
}
// 1 bytes = 2 character
// console.log(genSpecificLengthRandom(30));

const gen = () => {
  // console.log(process.argv[2])
  const defaultLength = 60;
  let inputLength = defaultLength;
  if (process.argv.length >= 3 && process.argv[2] && !isNaN(process.argv[2]) && Number(process.argv[2])%2 == 0) {
    inputLength = Number(process.argv[2])
  }
  const bytesLength = inputLength>>1;
  console.log(genSpecificLengthRandom(bytesLength));
}
gen()