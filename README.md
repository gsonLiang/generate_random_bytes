# generate_random_bytes

This is function for generate secret with specific length

## How to use

### default will gen length 60 secret
```shell=
yarn gen 
```
### default will gen secret with specific length
```shell=
yarn gen 
```
### secret format will be like

`[a-z0-9]{60}`

```shell=
b9ae3c82b79adbbc0382a6120bb8eca71e166636a9320194e511e77d8132
```